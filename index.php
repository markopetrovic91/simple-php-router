<?php
error_reporting(-1);
ini_set('display_errors', 1);
require 'core/bootstrap.php';

include Router::load('routes.php')
    ->direct(Request::uri());

?>