<?php
// define array of constants - available since PHP 7
$router->define([
    'php-router' => 'controllers/index.php',
    'php-router/about' => 'controllers/about.php',
    'php-router/about/culture' => 'controllers/about-culture.php',
    'php-router/contact' => 'controllers/contact.php'
]);

// $router->define('' , 'controllers/index.php');
// $router->define('about' , 'controllers/about.php');
// $router->define('about/culture' , 'controllers/about-culture.php');
// $router->define('contact' , 'controllers/contact.php');

?>